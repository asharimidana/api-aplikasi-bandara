Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  namespace :api do
    namespace :v1 do
      resources :users

      post "/login", to: "authentication#authenticate_user"

      resources :airlines
      resources :airline_classes
      resources :airports
      resources :orders
      resources :routes
      resources :schedules
    end
  end
end
