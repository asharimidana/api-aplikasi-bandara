User.create(
  [
    {
      name: "Ashari Midana",
      email: "asharimidana1@gmail.com",
      wa: "085395499307",
      password: "123456",
      role: 1,
      airline_id: nil
    },
    {
      name: "Agus Stiadi",
      email: "agussetiadi@gmail.com",
      wa: "085395499307",
      password: "123456",
      role: 1,
      airline_id: nil
    },
    {
      name: "Nur Karim",
      email: "nurkarim@gmail.com",
      wa: "085395499307",
      password: "123456",
      role: 1,
      airline_id: nil
    },
    {
      name: "khusnul yakin",
      email: "khusnul@mail.com",
      wa: "085395499307",
      password: "123456",
      role: 1,
      airline_id: nil
    }
  ]
)

Airline.create(
  [
    {
      name: "Lion Air",
      description: "menemani perjalanan anda dengan nyaman "
    },
    {
      name: "citilink",
      description: "menemani perjalanan anda dengan nyaman "
    },
    { name: "garuda", description: "menemani perjalanan anda dengan nyaman " }
  ]
)

Airport.create(
  [
    {
      name: "Bandar Halu Oleo",
      address: "Kendari ",
      province: "Sulawesi Tenggara"
    },
    {
      name: "Yogyakarta Internasional Airlport",
      address: "kulon progo",
      province: "Daerah Istimewah Yogyakarta"
    },
    {
      name: "Soekarno Hatta",
      address: "jakarta",
      province: "jakarta"
    },
    {
      name: "Sultan Hasanuddin",
      address: "Makassar",
      province: "Sulawesi Selatan"
    }
  ]
)
