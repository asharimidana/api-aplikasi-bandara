class RemoveColomnNameFromSchedulesTables < ActiveRecord::Migration[7.0]
  def change
    remove_column :schedules, :name
  end
end
