class CreateAllmigrates < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :wa
      t.string :password_digest
      t.integer :role
      t.bigint :airline_id

      t.timestamps
    end

    create_table :schedules do |t|
      t.bigint :user_id
      t.bigint :route_id
      t.bigint :airport_id
      t.string :name
      t.date :departure_time
      t.date :travel_time
      t.bigint :price
      t.integer :status

      t.timestamps
    end

    create_table :orders do |t|
      t.bigint :user_id
      t.bigint :schedule_id
      t.bigint :price
      t.integer :order_status

      t.timestamps
    end

    create_table :airports do |t|
      t.string :name
      t.string :address
      t.string :province

      t.timestamps
    end

    create_table :routes do |t|
      t.bigint :departure_id
      t.bigint :destination_id

      t.timestamps
    end

    create_table :airlines do |t|
      t.string :name
      t.string :description

      t.timestamps
    end

    create_table :airline_classes do |t|
      t.bigint :airline_id
      t.integer :airlinetype
      t.integer :capasity
      t.integer :cabin_weight
      t.integer :baggage_weight

      t.timestamps
    end
  end
end
