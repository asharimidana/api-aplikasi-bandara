class AddTypeDataTravelTimeSchedulesTables < ActiveRecord::Migration[7.0]
  def change
    add_column :schedules, :travel_time, :integer
  end
end
