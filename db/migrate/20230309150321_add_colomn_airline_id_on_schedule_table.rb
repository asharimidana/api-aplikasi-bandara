class AddColomnAirlineIdOnScheduleTable < ActiveRecord::Migration[7.0]
  def change
    add_column :schedules, :airline_class_id, :integer
  end
end
