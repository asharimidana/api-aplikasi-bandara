class Airline < ApplicationRecord
  has_many :airline_classes
  has_many :users, class_name: "User", foreign_key: "airline_id"
end
