class Schedule < ApplicationRecord
  belongs_to :user
  belongs_to :airline_class
  belongs_to :airport 
  belongs_to :route

  has_many :orders 

end
