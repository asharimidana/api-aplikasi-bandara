class User < ApplicationRecord
  has_secure_password

  has_many :schedules, dependent: :destroy
  has_many :orders, dependent: :destroy
  belongs_to :airline, optional: true

  validates :name, presence: true, length: { maximum: 50 }
  validates :email,
            presence: true,
            length: {
              maximum: 255
            },
            format: {
              with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
            }
  validates :wa, presence: true, uniqueness: true
  validates :role, presence: true
  validates :airline_id, presence: false

  def new_attributes
    {
      id: self.id,
      name: self.name,
      email: self.email,
      wa: self.wa,
      role: self.role,
      schedules: self.schedules,
      airline_id: self.airline_id,
      created_at: self.created_at
    }
  end
end
