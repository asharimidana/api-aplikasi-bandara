class Route < ApplicationRecord
  belongs_to :departure, class_name: "Airport"
  belongs_to :destination, class_name: "Airport"

  def new_atributes
    {
      id: self.id,
      departure: self.departure,
      destination: self.destination
    }
  end
end
