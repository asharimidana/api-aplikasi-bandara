class Airport < ApplicationRecord
  has_many :departure,
           dependent: :destroy,
           class_name: "Route",
           foreign_key: "departure_id"
  has_many :destination,
           dependent: :destroy,
           class_name: "Route",
           foreign_key: "destination_id"
end
