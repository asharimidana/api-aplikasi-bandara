class AirlineClass < ApplicationRecord
  belongs_to :airline
  has_many :schedules
end
