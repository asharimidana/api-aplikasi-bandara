module Api
  module V1
    class AirlineClassesController < ApplicationController
      before_action :authenticate_request!, except: :login
      rescue_from ActiveRecord::RecordNotFound, with: :not_found

      # GET /airlines
      def index
        @airline = AirlineClass.all
        render json: @airline
      end

      # GET /airlines/1
      def show
        @airline = AirlineClass.find(params[:id])
        render json: @airline
      end

      # POST /airlines
      def create
        @airline = AirlineClass.create(airline_params)
        if @airline.save
          render json: {
                   data: @airline,
                   message: "berhasil ditambahkan"
                 },
                 status: :created
        else
          render json: {
                   message: @airline.errors.objects.first.full_message
                 },
                 status: :unprocessable_entity
        end
      end

      # PATCH/PUT /airlines/1
      def update
        @airline = AirlineClass.find(params[:id])
        if @airline.update(airline_params)
          render json: { message: "airline berhasil dirubah" }, status: 200
        else
          render json: {
                   message: "gagal mengubah data"
                 },
                 status: :unprocessable_entity
        end
      end

      # DELETE /airlines/1
      def destroy
        @airline = AirlineClass.find(params[:id])
        if @airline.present?
          @airline.destroy!
          render json: { data: @airline, message: "berhasil dihapus" }
        else
          render json: {
                   error: @airline.errors.objects.first.full_message
                 },
                 status: 400
        end
      end

      private

      def airline_params
        params.permit(
          :airlinetype,
          :airline_id,
          :capasity,
          :cabin_weight,
          :baggage_weight
        )
      end

      def not_found
        render json: { message: "airline not found" }, status: 404
      end
    end
  end
end
