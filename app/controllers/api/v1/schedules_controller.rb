module Api
  module V1
    class SchedulesController < ApplicationController
      before_action :authenticate_request!, except: :login
      rescue_from ActiveRecord::RecordNotFound, with: :not_found

      # GET /routes
      def index
        @schedule = Schedule.all
        render json: @schedule
      end

      # GET /schedules/1
      def show
        @schedule = Schedule.find(params[:id])
        render json: @schedule
      end

      # POST /schedules
      def create
        @schedule = Schedule.create(schedule_params)
        if @schedule.save
          render json: {
                   data: @schedule,
                   message: "berhasil ditambahkan"
                 },
                 status: :created
        else
          render json: {
                   message: @schedule.errors.objects.first.full_message
                 },
                 status: :unprocessable_entity
        end
      end

      # PATCH/PUT /schedules/1
      def update
        @schedule = Schedule.find(params[:id])
        if @schedule.update(schedule_params)
          render json: { message: "schedule berhasil dirubah" }, status: 200
        else
          render json: {
                   message: "gagal mengubah data"
                 },
                 status: :unprocessable_entity
        end
      end

      # DELETE /schedules/1
      def destroy
        @schedule = Schedule.find(params[:id])
        if @schedule.present?
          @schedule.destroy!
          render json: { data: @schedule, message: "berhasil dihapus" }
        else
          render json: {
                   error: @schedule.errors.objects.first.full_message
                 },
                 status: 400
        end
      end

      private

      def schedule_params
        params.permit(
          :user_id,
          :route_id,
          :airport_id,
          :airline_class_id,
          :departure_id,
          :travel_time,
          :price,
          :status
        )
      end

      def not_found
        render json: { message: "route not found" }, status: 404
      end
    end
  end
end
