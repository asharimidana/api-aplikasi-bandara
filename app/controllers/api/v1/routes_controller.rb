module Api
  module V1
    class RoutesController < ApplicationController
      before_action :authenticate_request!, except: :login
      rescue_from ActiveRecord::RecordNotFound, with: :not_found

      # GET /routes
      def index
        @routes = Route.all
        render json:  @routes.map { |route| route.new_atributes }
      end

      # GET /routes/1
      def show
        @route = Route.find(params[:id])
        render json: @route
      end

      # POST /routes
      def create
        @route = Route.create(route_params)
        if @route.save
          render json: {
                   data: @route,
                   message: "berhasil ditambahkan"
                 },
                 status: :created
        else
          render json: {
                   message: @route.errors.objects.first.full_message
                 },
                 status: :unprocessable_entity
        end
      end

      # PATCH/PUT /routes/1
      def update
        @route = Route.find(params[:id])
        if @route.update(route_params)
          render json: { message: "route berhasil dirubah" }, status: 200
        else
          render json: {
                   message: "gagal mengubah data"
                 },
                 status: :unprocessable_entity
        end
      end

      # DELETE /routes/1
      def destroy
        @route = Route.find(params[:id])
        if @route.present?
          @route.destroy!
          render json: { data: @route, message: "berhasil dihapus" }
        else
          render json: {
                   error: @route.errors.objects.first.full_message
                 },
                 status: 400
        end
      end

      private

      def route_params
        params.permit( :departure_id, :destination_id)
      end

      def not_found
        render json: { message: "route not found" }, status: 404
      end
    end
  end
end
