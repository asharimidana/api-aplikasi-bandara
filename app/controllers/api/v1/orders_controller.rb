module Api
  module V1
    class OrdersController < ApplicationController
      before_action :authenticate_request!, except: :login
      rescue_from ActiveRecord::RecordNotFound, with: :not_found

      # GET /routes
      def index
        @orders = Order.all
        render json:  @orders
      end

      # GET /orders/1
      def show
        @order = Order.find(params[:id])
        render json: @order
      end

      # POST /orders
      def create
        @order = Order.create(order_params)
        if @order.save
          render json: {
                   data: @order,
                   message: "berhasil ditambahkan"
                 },
                 status: :created
        else
          render json: {
                   message: @order.errors.objects.first.full_message
                 },
                 status: :unprocessable_entity
        end
      end

      # PATCH/PUT /orders/1
      def update
        @order = Order.find(params[:id])
        if @order.update(order_params)
          render json: { message: "order berhasil dirubah" }, status: 200
        else
          render json: {
                   message: "gagal mengubah data"
                 },
                 status: :unprocessable_entity
        end
      end

      # DELETE /orders/1
      def destroy
        @order = Order.find(params[:id])
        if @order.present?
          @order.destroy!
          render json: { data: @order, message: "berhasil dihapus" }
        else
          render json: {
                   error: @order.errors.objects.first.full_message
                 },
                 status: 400
        end
      end

      private

      def order_params
        params.permit( :user_id, :schedule_id, :price, :order_status)
      end

      def not_found
        render json: { message: "order not found" }, status: 404
      end
    end
  end
end
