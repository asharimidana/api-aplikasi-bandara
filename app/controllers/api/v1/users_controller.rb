module Api
  module V1
    class UsersController < ApplicationController
      before_action :authenticate_request!, except: :login
      # rescue_from ActiveRecord::RecordNotFound, with: :not_found

      # GET /users
      def index
        @users = User.all
        render json: @users.map { |user| user.new_attributes }
      end

      # GET /users/1
      def show
        @user = User.find(params[:id])

        logger.debug "New article: "

        render json: @user.new_attributes
      end

      # POST /users
      def create
        @user = User.create(user_params)
        if @user.save
          render json: {
                   data: @user.new_attributes,
                   message: "berhasil ditambahkan"
                 },
                 status: :created
        else
          render json: {
                   message: @user.errors.objects.first.full_message
                 },
                 status: :unprocessable_entity
        end
      end

      # PATCH/PUT /users/1
      def update
        @user = User.find(params[:id])
        if @user.update(user_params)
          render json: { message: "user berhasil dirubah" }, status: 200
        else
          render json: {
                   message: "gagal mengubah data"
                 },
                 status: :unprocessable_entity
        end
      end

      # DELETE /users/1
      def destroy
        @user = User.find(params[:id])
        if @user.present?
          @user.destroy!
          render json: {
                   data: @user.new_attributes,
                   message: "berhasil dihapus"
                 }
        else
          render json: {
                   error: @user.errors.objects.first.full_message
                 },
                 status: 400
        end
      end

      def login
        @user = User.find_by(email: user_params[:email])

        if @user && @user.authenticate(user_params[:password_digest])
          token = encode_token({ user_id: @user.id })
          render json: { user: @user, token: token }, status: :ok
        else
          render json: {
                   error: "Invalid username or password"
                 },
                 status: :unprocessable_entity
        end
      end

      private

      def user_params
        params.permit(:name, :email, :wa, :password, :role, :airline_id)
      end

      def not_found
        render json: { message: "User not found" }, status: 404
      end
    end
  end
end
