class Api::V1::AirportsController < ApplicationController
  before_action :authenticate_request!, except: :login
  rescue_from ActiveRecord::RecordNotFound, with: :not_found

  # GET /airlines
  def index
    @airport = Airport.all
    render json: @airport
  end

  # GET /airports/1
  def show
    @airport = Airport.find(params[:id])
    render json: @airport
  end

  # POST /airports
  def create
    @airport = Airport.create(airport_params)
    if @airport.save
      render json: {
               data: @airport,
               message: "berhasil ditambahkan"
             },
             status: :created
    else
      render json: {
               message: @airport.errors.objects.first.full_message
             },
             status: :unprocessable_entity
    end
  end

  # PATCH/PUT /airports/1
  def update
    @airport = Airport.find(params[:id])
    if @airport.update(airport_params)
      render json: { message: "airport berhasil dirubah" }, status: 200
    else
      render json: {
               message: "gagal mengubah data"
             },
             status: :unprocessable_entity
    end
  end

  # DELETE /airports/1
  def destroy
    @airport = Airport.find(params[:id])
    if @airport.present?
      @airport.destroy!
      render json: { data: @airport, message: "berhasil dihapus" }
    else
      render json: {
               error: @airport.errors.objects.first.full_message
             },
             status: 400
    end
  end

  private

  def airport_params
    params.permit(:name, :address, :province)
  end

  def not_found
    render json: { message: "airline not found" }, status: 404
  end
end
